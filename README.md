gulfscei.rails.common
=========

Some common operations to prepare an environment to run a rails server with a PostgreSQL Backend

Requirements
------------

None

Role Variables
--------------

```
deploy_user:
app_path:

ruby: 2.3.0
rvm_path: /usr/local/rvm/bin/rvm
rails_deploy_env: staging
gemset_users: []

postgres_version: 9.4
postgres_listen_ip_address: 127.0.0.1
postgres_hba_lines: []
database_users: []

common_additional_packages: []

install_mailcatcher: yes
```

Dependencies
------------

None

Requirements File
-----------------

```
- src: git@gitlab.com:TridentUno/ansible-role-rails-deploy.git
  scm: git
  version: v0.2
  name: gulfscei.rails.common
```


Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: gulfscei.rails.common }

License
-------

MIT

Author Information
------------------

Contact `drward3@uno.edu` for information about this role
